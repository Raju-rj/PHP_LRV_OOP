<?php



  class Person{

      protected $name;
      protected $gender;
      protected $dob;

  }


  class Student extends  Person {

      protected $roll;
      protected $department;

      public function __construct()
      {

          echo "Im creating an object of Student Class <br>";

      }
      public function __destruct()
      {

          echo "Im destroying an object of Student Class <br>";

      }

      public function setData($studentInfo){

          $this->name = $studentInfo["Name"];
          $this->gender = $studentInfo["Gender"];
          $this->dob = $studentInfo["DOB"];
          $this->roll = $studentInfo["Roll"];
          $this->department = $studentInfo["Department"];


      }


      public function getData(){

          $studentInfo = array("Name"=>$this->name, "Gender"=>$this->gender, "DOB"=>$this->dob, "Roll"=>$this->roll, "Department"=>$this->department);

          return $studentInfo;
      }



  }


  $objStudent = new Student();
  $objStudent1= new Student();

  $studentInfo = array("Name"=>"Tushar Chowdhury", "Gender"=>"Male", "DOB"=>"04-08-1978", "Roll"=>"CSE232423", "Department"=>"Computer Science and Engineering");


  $objStudent->setData($studentInfo);

  $resultArray = $objStudent->getData();

  var_dump($resultArray);



